//1 - Declarar el vector
let vector = [1, "dos", true, false, 5, "seis"];

//a. Imprimir en la consola el vector
console.log("Vector:",vector);

//b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log("Primer elemento:",vector[0],"\nUltimo elemento:", vector[5]);

//c. Modificar el valor del tercer elemento
vector[2] = false;
console.log("Tercer elemento modificado:",vector);

//d. Imprimir en la consola la longitud del vector
console.log("Longitud del vector:",vector.length);

//e. Agregar un elemento al vector usando "push"
vector.push(7);
console.log("Elemento agregado al final usando push:",vector);

//f. Eliminar elemento del final e imprimirlo usando "pop"
let elim = vector.pop();
console.log("Elemento eliminado usando pop:",elim);

//g. Agregar un elemento en la mitad del vector usando "splice"
let mitad = Math.floor(vector.length / 2);
let nuevo = "nuevoElemento";
vector.splice(mitad, 0, nuevo);
console.log("Elemento agregado en la mitad usando splice:",vector);

//h. Eliminar el primer elemento usando "shift"
let primero = vector.shift();
console.log("Primer elemento eliminado usando shift:",vector);

//i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primero);
console.log("Elemento añadido usando unshift:",vector);