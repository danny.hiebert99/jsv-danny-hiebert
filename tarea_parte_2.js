//1 - Declarar el vector
let vector = [1, "dos", true, false, 5, "seis"];
//a. Imprimir en la consola cada valor usando "for"
console.log("\nUsando for:")
for (let i = 0; i < vector.length; i++) {
    console.log(vector[i]);
}

//b. Idem al anterior usando "forEach"
console.log("\nUsando forEach:")
vector.forEach(function(valor) {
    console.log(valor);
});

//c. Idem al anterior usando "map"
console.log("\nUsando map:")
vector.map(function(valor) {
    console.log(valor);
});

//d. Idem al anterior usando "while"
console.log("\nUsando while:")
let i = 0;
while (i < vector.length) {
    console.log(vector[i]);
    i++;
}

//e. Idem al anterior usando "for..of"
console.log("\nUsando for..of:")
for (let valor of vector) {
    console.log(valor);
}